#!/usr/bin/env python
"""

Process all tasks in a given input tasks file with Ray for job scheduling.

"""

import os
import ray
import rayfarmer.tools
import subprocess
import sys

# The directory containing this file
HERE = os.getcwd()


@ray.remote
def execute(args):

    # parse arguments
    # they need to be passed as a list for subprocess
    job_id = args[0]
    out_folder = args[1]
    keep_cwd = args[2]
    bash_cmd = args[3]

    # job execute directory
    cwd = os.path.join(HERE, out_folder, job_id)
    if not os.path.isdir(cwd):
        os.makedirs(cwd)

    # log file name
    stdout = os.path.join(cwd, "stdout.txt")
    out_stream = open(stdout, 'w')

    # error file name
    stderr = os.path.join(cwd, "stderr.txt")
    err_stream = open(stderr, 'w')

    # print
    print(bash_cmd)

    # execute
    if keep_cwd:
        p = subprocess.call(bash_cmd, stdout=out_stream,
                            stderr=err_stream, shell=True)
    else:
        p = subprocess.call(bash_cmd, stdout=out_stream,
                            stderr=err_stream, cwd=cwd, shell=True)

    # return job id and exit code
    return job_id, p


def print_progress(progress, total):
    sys.stdout.write(
        "Job progress: %d/%d [%d%%]   \r" % (
            progress, total, 100*progress/total))
    sys.stdout.flush()


def main(options):

    # initialize Ray
    if not options.local:
        # running on cluster
        ray.init(address=options.redis_address,
                 _redis_password=options.redis_password)
    else:
        # running locally
        ray.init(num_cpus=int(options.num_processes), ignore_reinit_error=True)

    # finished jobs from previous running
    finished_jobs = []
    out_file_path = os.path.join(HERE, options.output, "done.txt")
    if os.path.isfile(out_file_path):
        with open(out_file_path, 'r') as f:
            for line in f:
                finished_jobs += [line.strip()]

    # print finished jobs
    print(f"Found {len(finished_jobs)} already finished jobs.")

    # save tasks in an array
    # ignore already finished jobs
    tasks = []
    task_list = rayfarmer.tools.construct_task_list(
        options.config, options.input)
    for job in task_list.get_jobs():
        if job[0] not in finished_jobs:
            tasks += [[job[0], options.output, options.keep_cwd, job[1]]]
        else:
            print(f"Job {job[0]} already completed. Skipping..")

    # total number of jobs
    total = len(tasks)

    # end if no tasks
    if total == 0:
        return

    # prepare file with finished jobs
    if os.path.isfile(out_file_path):
        out_file = open(out_file_path, 'a')
        out_file.close()
    else:
        os.makedirs(os.path.join(HERE, options.output), exist_ok=True)
        out_file = open(out_file_path, 'w')
        out_file.close()

    # launch tasks with ray
    remaining_ids = []
    for task in tasks:
        if not options.dry_run:
            remaining_ids += [execute.remote(task)]
        else:
            print(task)

    # exit if dry run
    if options.dry_run:
        sys.exit(0)

    # wait for jobs to finish
    progress = 0
    print_progress(progress, total)
    while remaining_ids:
        ready_ids, remaining_ids = ray.wait(remaining_ids)
        for job, code in ray.get(ready_ids):
            if code == 0:
                out_file = open(out_file_path, 'a')
                out_file.write(f"{job}\n")
                out_file.close()
            else:
                print(f"job id {job} failed with exit code {code}")
        progress += len(ready_ids)
        print_progress(progress, total)

    # save timeline
    if options.timeline:
        ray.timeline(filename="timeline.json")


if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser(description=globals()["__doc__"])

    # ----------------------------------------------------
    # Parse input
    # ----------------------------------------------------
    parser.add_option('-i', '--input',
                      action="store", dest="input",
                      help="input tasks file")
    parser.add_option('-c', '--config',
                      action="store", dest="config",
                      help="job config file",
                      default="default")
    parser.add_option('-o', '--output',
                      action="store", dest="output",
                      help="output folder",
                      default="jobs")
    parser.add_option('-l', '--local',
                      action="store_true", dest="local",
                      help="rune locally on one node",
                      default=False)
    parser.add_option('-p', '--num-processes',
                      action="store", dest="num_processes",
                      help="number of processes for local running")
    parser.add_option('-k', '--keep-cwd',
                      action="store_true", dest="keep_cwd",
                      help="run the jobs from the base directory")
    parser.add_option('--redis-address',
                      action="store", dest="redis_address",
                      help="ip address of the Ray head node")
    parser.add_option('--redis-password',
                      action="store", dest="redis_password",
                      help="password of the Redis server")
    parser.add_option('--timeline',
                      action="store_true", dest="timeline",
                      help="save Ray timeline")
    parser.add_option('--dry-run',
                      action="store_true", dest="dry_run",
                      help="only print out the jobs")

    # parse input arguments
    options, _ = parser.parse_args()

    # set default ip and password for non-local running
    if not options.local:
        if not options.redis_address:
            options.redis_address = "%s:%s" % (
                os.environ["RAY_HEAD_IP"], os.environ["RAY_REDIS_PORT"])
            options.redis_password = os.environ["RAY_REDIS_PASSWORD"]

    # required arguments
    assert options.input
    if not options.local:
        assert options.redis_password
        assert options.redis_address
        assert not options.num_processes
    else:
        assert options.num_processes

    # print
    print(f"Processing tasks from input file {options.input}")

    # run main
    main(options)
