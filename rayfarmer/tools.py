from typing import Dict
import os
import pathlib
import rayfarmer.task
import yaml

# The base directory
BASE = pathlib.Path(__file__).parent.parent


def find_file_path(name: str) -> str:
    if os.path.isfile(name):
        return name
    elif os.path.isfile(os.path.join(BASE, "config", name)):
        return os.path.join(BASE, "config", name)
    elif os.path.isfile(os.path.join(BASE, "test", name)):
        return os.path.join(BASE, "test", name)
    else:
        raise IOError(f"File {name} not found")


def parse_yaml_file(name: str) -> Dict:
    with open(name, 'r') as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)


def construct_task_list(config: str,
                        input: str) -> rayfarmer.task.BaseTaskList:

    # parse config
    if not config.endswith(".yaml"):
        config = config + ".yaml"
    conf = parse_yaml_file(find_file_path(config))

    # check for valid config
    assert "type" in conf
    assert hasattr(rayfarmer.task, conf["type"])

    # construct the TaskList object
    task_list = getattr(rayfarmer.task, conf["type"])(conf)

    # read in the list of jobs from the txt file
    task_list.read_jobs(find_file_path(input))

    return task_list
